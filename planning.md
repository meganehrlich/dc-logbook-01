Create app for Dolphin Swimming and Rowing Club
start with simple project to act as log book
boat model
other boat model
user model

[x] create home page:
  - listview 
  - public, everyone can see
  - login option at top of page
  - signup option at top of page
  - link to return to top

[x] create members logbook page
  - listview
  - login required
  - logout option at top
  - navigation to other pages
  - shows boats, weight, capacity, and availability
  - option to "check in/checkout" boats

[x] Add comments for boats

[x] add other boats
  - list on public page
  - list on checkout page

List individual boats on other boat detail page

Add duration field to checkout
  - other members can check online to see
    if boat will be there when they come down

Add member profile

Add calendar

[] Create member events page
  - checkin/register for events?

[]create individual member page
    - personal calendar ?
      - see other members planned trips?
    - log boats used
    - logs miles swum


  * * Endless possibilities!!! * *

[] Learn how to write tests for Django framework
[] Add complexity to accounts
  [] Add staff login + basic user login
  