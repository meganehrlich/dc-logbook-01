from django.contrib import admin
from .models import Rowboat, Comment, OtherBoat

admin.site.register(Rowboat)
admin.site.register(Comment)
admin.site.register(OtherBoat)
