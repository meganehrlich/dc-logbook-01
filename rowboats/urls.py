from django.urls import path
from .views import (list_rowboats, public_view, checkout, checkin, 
                    create_comment, rowboat_detail, otherboat_detail, 
                    ocheckout, ocheckin,
                    )


urlpatterns = [
    path('rowboats/', list_rowboats, name='list_rowboats'),
    path('', public_view, name='public_view'),
    path('<int:pk>/', rowboat_detail, name='rowboat_detail'),
    path('<int:pk>/checkout/', checkout, name='checkout'),
    path('<int:pk>/checkin/', checkin, name='checkin'),
    path('<int:pk>/ocheckout/', ocheckout, name='ocheckout'),
    path('<int:pk>/ocheckin/', ocheckin, name='ocheckin'),
    path('rowboats/comment/', create_comment, name='create_comment'),
    path('otherboats/<int:pk>/', otherboat_detail, name='otherboat_detail')

]
