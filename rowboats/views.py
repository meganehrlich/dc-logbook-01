from django.shortcuts import render, redirect, get_object_or_404
from .models import Rowboat, OtherBoat
from django.contrib.auth.decorators import login_required
from .forms import CommentForm


def public_view(request):
    rowboats = Rowboat.objects.all()
    otherboats = OtherBoat.objects.all()
    context = {"rowboats": rowboats, "otherboats": otherboats}
    return render(request, "rowing/list.html", context)


@login_required
def list_rowboats(request):
    rowboats = Rowboat.objects.all()
    otherboats = OtherBoat.objects.all()
    context = {
        "rowboats": rowboats,
        "otherboats": otherboats,
    }
    return render(request, "rowing/rowboats.html", context)


@login_required
def rowboat_detail(request, pk):

    context = {
        "boat": Rowboat.objects.get(pk=pk) if Rowboat else None,
    }
    return render(request, "rowing/detail.html", context)


@login_required
def otherboat_detail(request, pk):
    context = {"boat": OtherBoat.objects.get(pk=pk) if OtherBoat else None}
    return render(request, "rowing/otherdetails.html", context)


@login_required
def checkout(request, pk):
    available = get_object_or_404(Rowboat, pk=pk)
    if request.method == "POST":
        available.availability = False
        available.availability = True
        available.save()
        return redirect("/rowing/rowboats/", pk=pk)
    return render(request, "rowing/rowboats.html", {"available": available})


@login_required
def checkin(request, pk):
    available = get_object_or_404(Rowboat, pk=pk)
    if request.method == "POST":
        available.availability = True
        available.availability = False
        available.save()
        return redirect("/rowing/rowboats/", pk=pk)
    return render(request, "rowing/rowboats.html", {"available": available})


@login_required
def ocheckout(request, pk):
    available = get_object_or_404(OtherBoat, pk=pk)
    if request.method == "POST":
        available.availability = False
        available.availability = True
        available.save()
        return redirect("/rowing/rowboats/", pk=pk)
    return render(request, "rowing/rowboats.html", {"available": available})


@login_required
def ocheckin(request, pk):
    available = get_object_or_404(OtherBoat, pk=pk)
    if request.method == "POST":
        available.availability = True
        available.availability = False
        available.save()
        return redirect("/rowing/rowboats/", pk=pk)
    return render(request, "rowing/rowboats.html", {"available": available})


def redirect_view(request):
    response = redirect("public_view")
    return response


@login_required
def create_comment(request):
    context = {}
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(list_rowboats)
        else:
            form = CommentForm

        context = {"form": form}

    return render(request, "rowing/comment.html", context)
