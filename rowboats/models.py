from django.db import models
from django.conf import settings


class Rowboat(models.Model):
    name = models.CharField(max_length=100)
    capacity = models.SmallIntegerField(null=True, blank=True)
    image = models.URLField(null=True, blank=True)
    weight = models.SmallIntegerField(null=True, blank=True)
    availability = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class OtherBoat(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    image = models.URLField(null=True, blank=True)
    availability = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Comment(models.Model):
    member = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="comments", on_delete=models.CASCADE
    )
    boat = models.ForeignKey(Rowboat, related_name="comments", on_delete=models.CASCADE)
    comment = models.TextField()

    def __str__(self):
        return self.boat
