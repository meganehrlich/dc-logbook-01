from django.contrib import admin
from django.urls import path, include
from rowboats.views import redirect_view

urlpatterns = [
    path("admin/", admin.site.urls),
    path("rowing/", include("rowboats.urls")),
    path("", redirect_view, name="home"),
    path("accounts/", include("accounts.urls")),
]
