from django.test import SimpleTestCase


class FeatureTests(SimpleTestCase):
    def test_expenses_receipt_created(self):
        try:
            from rowboats.models import Rowboat 
        except ModuleNotFoundError:
            self.fail("Could not find the Django project 'expenses'")
