# DC Logbook 01

First attempt of DC boat reservation logbook 
Built during Hack Reactor Software Engineering Immersive
Periodically updated as I continue learning and growing as a software engineer

## Usage
This site was created to be used by my local rowing club as a virtual logbook to check out boats and see if the member's favorite boat is available.

## Authors and acknowledgment
I (Megan Ehrlich) am the sole contributor to this project

## License
This is a personal project and copyright on pages is purely aesthetic

## Project status
Next up: ....
